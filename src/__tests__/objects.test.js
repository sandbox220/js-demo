import {create, map} from "../objects";

/**
 * Create an object from an array.
 * Each element of an input array should be both the key and the value of resulting object
 * Input:
 *   - array
 * Output:
 *   - new object
 *
 * To be discovered: setting/changing the attributes (field) of JS objects
 */
describe('create object', () => {
    it('empty', () => {
        expect(create([]))
            .toMatchObject({})
    });

    it('object with ints', () => {
        expect(create([1, 2, 3]))
            .toMatchObject({"1": 1, "2": 2, "3": 3})
    });

    it('complex object', () => {
        expect(create([42, null, "foo"]))
            .toMatchObject({"42": 42, "null": null, "foo": "foo"})
    });

    it('should be non-destructive', () => {
        const array = [42, null, "foo"];
        create(array);
        expect(array).toEqual([42, null, "foo"])
    });
});


/**
 * Renames fields of an input object. Accepts input object and config. For each field of an input object
 * if the key is present in config, rename it to a value, specified by config, otherwise leave unchanged.
 * Inputs:
 *   - input object to be processed
 *   - object that represents rename config. The value of each key is a new name for this key in the input object
 * Output:
 *   - new object with renamed fields
 *
 * To be discovered: for loop on objects ( for (let field in {}) { console.log(field) })
 */
describe('map object', () => {
   it('empty map', () => {
      expect(rename({"a": 1, "b": "foo"}, {}))
          .toMatchObject({"a": 1, "b": "foo"})
   });

   it('empty object', () => {
       expect(rename({}, {"a": 1, "b": "foo"}))
           .toMatchObject({})
   });

   it('empty map and object', () => {
       expect(rename({}, {}))
           .toMatchObject({})
   });

    it('map all fields', () => {
        const object = {"a": 1, "b": 42, "c": "foo"};
        const mapper = {"a": "newA", "c": "foo", "b": "a"};
        expect(rename(object, mapper))
            .toMatchObject({"newA": 1, "a": 42, "foo": "foo"})
    });

    it('map contains more fields', () => {
        const object = {"a": 1, "b": 42};
        const mapper = {"a": "newA", "c": "foo", "b": "a"};
        expect(rename(object, mapper))
            .toMatchObject({"newA": 1, "a": 42})
    });

    it('should be non-destructive', () => {
        const object = {"a": 1, "b": 42};
        const mapper = {"a": "c", "b": "d"};
        rename(object, mapper);
        expect(object).toMatchObject({"a": 1, "b": 42});
        expect(mapper).toMatchObject({"a": "c", "b": "d"})
    });
});


/**
 * Duplicates fields on an input object.
 * Inputs:
 *   - input object to be processed
 *   - object that represents a config. Each key represents a field name to be duplicated.
 *     Each value represents a list of fields to be created. Value could be either an array or
 *     a single string that should be processed as an array of one element.
 * Output:
 *   - new object with duplicated fields
 */
describe('map object', () => {
    it('empty map', () => {
        expect(duplicate({"a": 1, "b": "foo"}, {}))
            .toMatchObject({"a": 1, "b": "foo"})
    });

    it('empty object', () => {
        expect(duplicate({}, {"a": 1, "b": "foo"}))
            .toMatchObject({})
    });

    it('empty map and object', () => {
        expect(duplicate({}, {}))
            .toMatchObject({})
    });

    it('simple', () => {
        const object = {"a": 1};
        const mapper = {"a": "b"};
        expect(duplicate(object, mapper))
            .toMatchObject({"a": 1, "b": 1})
    });

    it('single field', () => {
        const object = {"a": 1};
        const mapper = {"a": ["b", "c"]};
        expect(duplicate(object, mapper))
            .toMatchObject({"a": 1, "b": 1, "c": 1})
    });

    it('multiple fields', () => {
        const object = {"a": 1, "b": 42};
        const mapper = {"a": ["a2", "a3"], "b": "b2"};
        expect(duplicate(object, mapper))
            .toMatchObject({"a": 1, "a2": 1, "a3": 1, "b": 42, "b2": 42})
    });

    it('some fields', () => {
        const object = {"a": 1, "b": 42};
        const mapper = {"b": "b2"};
        expect(duplicate(object, mapper))
            .toMatchObject({"a": 1, "b": 42, "b2": 42})
    });

    it('no fields', () => {
        const object = {"a": 1, "b": 42};
        const mapper = {"c": "c2"};
        expect(duplicate(object, mapper))
            .toMatchObject({"a": 1, "b": 42})
    });

    it('should be non-destructive', () => {
        const object = {"a": 1, "b": 42};
        const mapper = {"a": "c", "b": "d"};
        duplicate(object, mapper);
        expect(object).toMatchObject({"a": 1, "b": 42});
        expect(mapper).toMatchObject({"a": "c", "b": "d"})
    });
});