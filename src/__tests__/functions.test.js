import {map, incrementer, filter, reduce} from "../functions";

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
 * To be implemented (some simple analogue).
 * Inputs:
 *   - array to be processed
 *   - function to be applied to each element. Function accepts two args: current element and (optional) current index
 * Output:
 *   - new array with elements created from input array by applying specified function
 *
 * To be discovered: varargs (variable lengths arguments list), high-order functions, lambda functions
 * Demo:
 * function outer(func) {
 *     return func('will be printed')
 * }
 * outer(console.log) // -> console.log('will be printed')
 */
describe('map', () => {
    it('empty', () => {
        expect(map([], x => x)).toBeEqual([])
    });

    it('no change', () => {
        expect(map([1, 2, 3], x => x)).toBeEqual([1, 2, 3])
    });

    it('function', () => {
        function increment(number) {
            return number + 1
        }

        const array = [-2, 1, 0, 3, 0];
        expect(map(array, increment))
            .toBeEqual([-1, 2, 1, 4, 1])
    });

    it('lambda', () => {
        const array = [-2, 1, 0, 3, 0];
        expect(map(array, x => x + 3))
            .toBeEqual([1, 4, 3, 6, 3])
    });

    it('with index', () => {
        const array = [-2, 1, 4, 3, 0];
        expect(map(array, (x, i) => x + i))
            .toBeEqual([-2, 2, 6, 6, 4])
    });

    it('should be non-destructive', () => {
        const array = [-2, 1, 4, 3, 0];
        map(array, (x, i) => x + i);
        expect(array).toBeEqual([-2, 1, 4, 3, 0])
    });
});

/**
 * Implement a function that returns other function.
 * Input:
 *   - number to be used in inner function
 * Output:
 *   - function that returns a sum of it's argument and an outer number
 *
 * To be discovered: nested functions, high-order functions, lambda functions
 * 1)
 * return function (x) { return x + 1 }
 * 2)
 * let func = function (x) { return x + 1 }
 * return func
 * 3)
 * return x => x + 1;
 */
describe('incrementer', function () {
    it('demo 1', () => {
        const adder = incrementer(4);
        expect(adder(0)).toBe(4);
        expect(adder(5)).toBe(9);
        expect(adder(-5)).toBe(1);
    });

    it('demo 2', () => {
        const adder = incrementer(3.5);
        expect(adder(0)).toBe(3.5);
        expect(adder(3.5)).toBe(7);
        expect(adder(-3.5)).toBe(0);
        expect(adder(-5)).toBe(-2);
    });
});


/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
 *
 * The same as map. Accepts an array and a function that accepts an element and (optional) index and returns boolean value
 */
describe('filter', () => {
    it('true', () => {
        const array = [1, 2, 3];
        expect(filter(array, () => true))
            .toEqual([1, 2, 3])
    });

    it('false', () => {
        const array = [1, 2, 3];
        expect(filter(array, () => false))
            .toEqual([])
    });

    it('filter', () => {
        const array = [1, 2, 3, 4, 5, 6];
        expect(filter(array, x => x % 2 === 0))
            .toEqual([2, 4, 6])
    });

    it('with index', () => {
        const array = [-1, 1, 3, 3, 4, 6, 0];
        expect(filter(array, (x, i) => x === i))
            .toEqual([1, 3, 4])
    });

    it('should be non-destructive', () => {
        const array = [-2, 1, 4, 3, 0];
        filter(array, _ => false);
        expect(array).toBeEqual([-2, 1, 4, 3, 0])
    });
});


/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
 */
describe('reduce', () => {
    it('simple', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc + value)).toBe(20)
    });

    it('with initial value 1', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc + value, 0)).toBe(20)
    });

    it('with initial value 2', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc + value, 7)).toBe(27)
    });

    it('with index', () => {
        const array = [2, 4, 6, 8];
        const expected =  2 * 0 + 4 * 1 + 6 * 2 + 8 * 3;
        expect(reduce(array, (acc, value, index) => acc + value * index)).toBe(expected)
    });

    it('with index and initial value 1', () => {
        const array = [2, 4, 6, 8];
        const expected =  2 * 0 + 4 * 1 + 6 * 2 + 8 * 3;
        expect(reduce(array, (acc, value, index) => acc + value * index, 0)).toBe(expected)
    });

    it('with index and initial value 2', () => {
        const array = [2, 4, 6, 8];
        const expected =  2 * 0 + 4 * 1 + 6 * 2 + 8 * 3;
        expect(reduce(array, (acc, value, index) => acc + value * index, 5)).toBe(expected + 5)
    });

    it('string', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc + '' + value)).toBe('2468')
    });

    it('string with index', () => {
        const array = [2, 4, 6, 8];
        // NOTE: index should NOT BE processed for the first element
        expect(reduce(array, (acc, value, index) => acc + '' + value + index)).toBe('2416283')
    });

    it('string with index and initial value', () => {
        const array = [2, 4, 6, 8];
        // NOTE: index MUST BE processed for the first element
        expect(reduce(array, (acc, value, index) => acc + '' + value + index, 9)).toBe('920416283')
    });

    it('string with initial value', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc + '' + value, 73)).toBe('732468')
    });

    it('dirty implicit cast to string', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc + value, '')).toBe('2468')
    });

    it('last processed', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => value)).toBe(8)
    });

    it('last processed with initial', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => value, 42)).toBe(8)
    });

    it('first processed', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, (acc, value) => acc)).toBe(2)
    });

    it('first processed with initial', () => {
        const array = [2, 4, 6, 8];
        expect(reduce(array, acc => acc, 42)).toBe(42)
    });

    it('should be non-destructive', () => {
        const array = [-2, 1, 4, 3, 0];
        reduce(array, (acc, value) => value);
        expect(array).toBeEqual([-2, 1, 4, 3, 0])
    });
});