import Matrix, {fill, print} from "../matrix";

const filler = (row, column) => row * column;
const messages = [];

const config = {
    "rows": 10,
    "columns": 4,
    "filler": filler,
    "printer": messages.push
};

describe('complex', function () {
    it('create', function () {
        const matrix = new Matrix(config);
        expect(matrix.size).toMatchObject({"rows": config.rows, "columns": config.columns});
        expect(matrix.rows).toBe(config.rows);
        expect(matrix.columns).toBe(config.columns);
    });

    it('filler', () => {
        const matrix = new Matrix(config);
        matrix.fill();
        matrix.data.forEach((row, i) => row.forEach((elem, j) => expect(elem).toBe(filler(i, j))))
    });

    it('custom filler', () => {
        const matrix = new Matrix(config);
        const customFiller = (row, column) => row * 100 + column;
        matrix.fill(customFiller);
        matrix.data.forEach((row, i) => row.forEach((elem, j) => expect(elem).toBe(customFiller(i, j))))
    });

    it('printer', () => {
        const matrix = new Matrix(config);
        matrix.fill();
        matrix.print();
        for (let i = 0; i < config.rows; i++) {
            for (let j = 0; j < config.columns; j++) {
                expect(messages[i * config.columns + j]).toBe(filler(i, j))
            }
        }
    });

    it('custom printer', () => {
        const matrix = new Matrix(config);
        const otherMessages = [];
        matrix.fill();
        matrix.print(elem => otherMessages.push(elem));
        for (let i = 0; i < config.rows; i++) {
            for (let j = 0; j < config.columns; j++) {
                expect(otherMessages[i * config.columns + j]).toBe(filler(i, j))
            }
        }
    });

    it('external filler', () => {
        const matrix = new Matrix(config);
        const customFiller = (row, column) => row * 100 + column;
        fill(matrix, customFiller);
        matrix.data.forEach((row, i) => row.forEach((elem, j) => expect(elem).toBe(customFiller(i, j))))
    });

    it('external printer', () => {
        const matrix = new Matrix(config);
        const otherMessages = [];
        matrix.fill();
        print(matrix, elem => otherMessages.push(elem));
        for (let i = 0; i < config.rows; i++) {
            for (let j = 0; j < config.columns; j++) {
                expect(otherMessages[i * config.columns + j]).toBe(filler(i, j))
            }
        }
    });
});

